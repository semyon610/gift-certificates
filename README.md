# Educational project: Gift certificates

## REST API

### Business requirements

1. Develop web service for Gift Certificates system with the following entities (many-to-many):
![schema.png](schema.png)
- CreateDate, LastUpdateDate - format ISO 8601
- Duration - in days (expiration period)

2. The system expose REST APIs to perform the following operations:

- CRUD operations for GiftCertificate. If new tags are passed during creation/modification – they are created in
  the DB. For update operation - update only fields, that pass in request, others should not be updated. Batch insert is
  out of scope.
- CRD operations for Tag.
- Get certificates with tags (all params are optional and can be used in conjunction):
    - by tag name (ONE tag)
    - search by part of name/description (can be implemented, using DB function call)

### Technologies
1. Java 11
2. Spring MVC, Boot, Data
3. JPA, Hibernate
4. PostgreSQL
5. Redis
6. Kafka
7. Liquibase
8. Spring Cache
9. MapStruct
10. Lombok
11. Docker
12. Maven
13. Swagger 
14. Mockito
15. Test container

### Note
Radisson and Kafka are used for study purposes.
