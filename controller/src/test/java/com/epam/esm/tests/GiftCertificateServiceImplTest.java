package com.epam.esm.tests;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.GiftCertificateNotFoundException;
import com.epam.esm.exception.GiftCertificateNotUniqueNameException;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.stream.Collectors;

@Transactional
public class GiftCertificateServiceImplTest extends AbstractTestContainer {
    private final GiftCertificateService giftCertificateService;
    private final TagService tagService;
    private final RedissonClient redissonClient;
    private static final String NOT_EXISTING_TAG_NAME = "tagName";
    private static final String GIFT_CERTIFICATE_NAME = "giftCertificateName";
    private static final String DESCRIPTION = "description";
    private static final Double PRICE = 1.00;
    private static final Integer DURATION = 10;
    private static final Long EXISTING_TAG_ID = 1L;
    private static final Long EXISTING_GIFT_CERTIFICATE_ID = 1L;
    private static final Long NOT_EXISTING_GIFT_CERTIFICATE_ID = 100L;
    private static final Integer TEN_SECOND = 10000;
    private static final Integer TWENTY_SECOND = 20000;

    @Autowired
    public GiftCertificateServiceImplTest(GiftCertificateService giftCertificateService, TagService tagService,
                                          RedissonClient redissonClient) {
        this.giftCertificateService = giftCertificateService;
        this.tagService = tagService;
        this.redissonClient = redissonClient;
    }

    @AfterEach
    public void clear() {
        redissonClient.getKeys().flushdb();
    }

    @Test
    public void when_getGiftCertificateById_thenShould_returnEntity() {
        Assertions.assertDoesNotThrow(() -> giftCertificateService.get(EXISTING_GIFT_CERTIFICATE_ID));
    }

    @Test
    public void when_getGiftCertificateById_thenShould_throwException() {
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateService.get(NOT_EXISTING_GIFT_CERTIFICATE_ID));
    }

    @Test
    public void when_getAllGiftCertificate_thenShould_returnListOfEntities() {
        Assertions.assertDoesNotThrow(giftCertificateService::getAll);
    }

    @Test
    public void when_addGiftCertificate_thenShould_returnEntity() throws InterruptedException {
        TagDto existingTag = tagService.get(EXISTING_TAG_ID);
        TagDto newTag = TagDto.builder().name(NOT_EXISTING_TAG_NAME).build();

        Assertions.assertEquals(2, giftCertificateService.getAll().size());
        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());

        giftCertificateService.add(GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .duration(DURATION)
                .description(DESCRIPTION)
                .tags(Set.of(existingTag, newTag))
                .build());

        Assertions.assertEquals(3, giftCertificateService.getAll().size());
        Thread.sleep(TWENTY_SECOND);
        Assertions.assertEquals(1, giftCertificateService.getrList().size());
        Assertions.assertEquals(1, tagService.getrList().size());
        Thread.sleep(TEN_SECOND);
        Assertions.assertEquals(6, tagService.getAll().size());
    }

    @Test
    public void when_addGiftCertificate_thenShould_throwException() {
        Assertions.assertEquals(2, giftCertificateService.getAll().size());
        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());

        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .name(giftCertificateService.get(EXISTING_GIFT_CERTIFICATE_ID).getName())
                        .price(PRICE)
                        .duration(DURATION)
                        .description(DESCRIPTION)
                        .tags(Set.of())
                        .build()));

        Assertions.assertEquals(2, giftCertificateService.getAll().size());
        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());
    }

    @Test
    public void when_updateGiftCertificate_thenShould_returnEntity() throws InterruptedException {
        TagDto existingTag = tagService.get(EXISTING_GIFT_CERTIFICATE_ID);
        TagDto newTag = TagDto.builder().name(NOT_EXISTING_TAG_NAME).build();

        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());

        GiftCertificateDto existing = giftCertificateService.get(EXISTING_GIFT_CERTIFICATE_ID);
        GiftCertificateDto forUpdate = GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .duration(DURATION)
                .description(DESCRIPTION)
                .tags(Set.of(existingTag, newTag))
                .build();

        Assertions.assertNotEquals(existing.getName(), forUpdate.getName());
        Assertions.assertNotEquals(existing.getPrice(), forUpdate.getPrice());
        Assertions.assertNotEquals(existing.getDuration(), forUpdate.getDuration());
        Assertions.assertNotEquals(existing.getDescription(), forUpdate.getDescription());
        Assertions.assertNotEquals(existing.getTags().size(), forUpdate.getTags().size());

        GiftCertificateDto updated = giftCertificateService.update(EXISTING_GIFT_CERTIFICATE_ID, forUpdate);

        Assertions.assertEquals(forUpdate.getName(), updated.getName());
        Assertions.assertEquals(forUpdate.getPrice(), updated.getPrice());
        Assertions.assertEquals(forUpdate.getDuration(), updated.getDuration());
        Assertions.assertEquals(forUpdate.getDescription(), updated.getDescription());
        Assertions.assertTrue(forUpdate.getTags().stream()
                .map(TagDto::getName)
                .allMatch(name -> updated.getTags().stream()
                        .map(TagDto::getName)
                        .collect(Collectors.toSet())
                        .contains(name)));

        Thread.sleep(TEN_SECOND);
        Assertions.assertEquals(1, giftCertificateService.getrList().size());
        Assertions.assertEquals(1, tagService.getrList().size());
        Thread.sleep(TEN_SECOND);
        Assertions.assertEquals(6, tagService.getAll().size());
    }

    @Test
    public void when_updateGiftCertificate_thenShould_throwException() {
        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());

        GiftCertificateDto existing = giftCertificateService.get(EXISTING_GIFT_CERTIFICATE_ID);
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateService.update(EXISTING_GIFT_CERTIFICATE_ID, existing));
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateService.get(NOT_EXISTING_GIFT_CERTIFICATE_ID));

        Assertions.assertEquals(0, giftCertificateService.getrList().size());
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());
    }

    @Test
    public void when_deleteGiftCertificateById_thenShould_returnNothing() {
        Assertions.assertDoesNotThrow(() -> giftCertificateService.delete(EXISTING_GIFT_CERTIFICATE_ID));
    }

    @Test
    public void when_deleteGiftCertificateById_thenShould_throwException() {
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateService.delete(NOT_EXISTING_GIFT_CERTIFICATE_ID));
    }
}
