package com.epam.esm.tests;

import com.epam.esm.dao.entity.Tag;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.TagNotFoundException;
import com.epam.esm.exception.TagNotUniqueNameException;
import com.epam.esm.service.TagService;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Transactional
public class TagServiceImplTest extends AbstractTestContainer {
    private final TagService tagService;
    private final RedissonClient redissonClient;
    private static final Long EXISTING_GIFT_CERTIFICATE_ID = 1L;
    private static final String NOT_EXISTING_TAG_NAME = "tagName";
    private static final Long NOT_EXISTING_TAG_ID = 100L;
    private static final Long EXISTING_TAG_ID = 1L;
    private static final Integer TEN_SECOND = 10000;

    @Autowired
    public TagServiceImplTest(TagService tagService, RedissonClient redissonClient) {
        this.tagService = tagService;
        this.redissonClient = redissonClient;
    }

    @AfterEach
    public void clear() {
        redissonClient.getKeys().flushdb();
    }

    @Test
    public void when_getTagById_thenShould_returnEntity() {
        Assertions.assertDoesNotThrow(() -> tagService.get(EXISTING_TAG_ID));
    }

    @Test
    public void when_getTagById_thenShould_throwException() {
        Assertions.assertThrows(TagNotFoundException.class, () -> tagService.get(NOT_EXISTING_TAG_ID));
    }

    @Test
    public void when_getAllTags_thenShould_returnEntities() {
        Assertions.assertDoesNotThrow(tagService::getAll);
    }

    @Test
    public void when_addTag_thenShould_returnEntity() throws InterruptedException {
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());
        tagService.add(TagDto.builder().name(NOT_EXISTING_TAG_NAME).build());
        Assertions.assertEquals(6, tagService.getAll().size());
        Thread.sleep(TEN_SECOND);
        Assertions.assertEquals(1, tagService.getrList().size());
    }

    @Test
    public void when_addTag_thenShould_throwException() {
        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());

        TagDto existingTag = tagService.get(EXISTING_TAG_ID);
        Assertions.assertThrows(TagNotUniqueNameException.class, () -> tagService.add(existingTag));

        Assertions.assertEquals(5, tagService.getAll().size());
        Assertions.assertEquals(0, tagService.getrList().size());
    }

    @Test
    public void when_deleteTagById_thenShould_returnNothing() {
        Assertions.assertDoesNotThrow(() -> tagService.delete(EXISTING_TAG_ID));
    }

    @Test
    public void when_deleteTagById_thenShould_throwException() {
        Assertions.assertThrows(TagNotFoundException.class, () -> tagService.delete(NOT_EXISTING_TAG_ID));
    }

    @Test
    public void when_findAllTagsByGiftCertificateId_thenShould_returnListOfEntities() {
        Assertions.assertDoesNotThrow(() -> tagService.findAllByGiftCertificateId(EXISTING_GIFT_CERTIFICATE_ID));
    }

    @Test
    public void when_findAllTagsByNotExistingGiftCertificateId_thenShould_returnEmptySet() {
        Assertions.assertEquals(Set.of(), tagService.findAllByGiftCertificateId(1000L));
    }

    @Test
    public void when_saveNotExistingTags_thenShould_returnSetOfEntities() {
        TagDto existingTag = tagService.get(EXISTING_GIFT_CERTIFICATE_ID);
        TagDto newTag = TagDto.builder().name(NOT_EXISTING_TAG_NAME).build();

        Set<Tag> savedTags = tagService.saveNotExistingTags(Set.of(existingTag, newTag));
        Assertions.assertEquals(2, savedTags.size());
        Assertions.assertTrue(savedTags.stream().anyMatch(tag -> tag.getName().equals(newTag.getName())));
    }
}
