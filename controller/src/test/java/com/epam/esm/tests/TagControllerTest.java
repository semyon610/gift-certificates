package com.epam.esm.tests;

import com.epam.esm.controller.TagController;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.TagNotFoundException;
import com.epam.esm.exception.TagNotUniqueNameException;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@Transactional
public class TagControllerTest extends AbstractTestContainer {
    private final TagController tagController;
    private final RedissonClient redissonClient;
    private static final String NOT_EXISTING_TAG_NAME = "tagName";
    private static final Long NOT_EXISTING_TAG_ID = 100L;
    private static final Long EXISTING_TAG_ID = 1L;

    @Autowired
    public TagControllerTest(TagController tagController, RedissonClient redissonClient) {
        this.tagController = tagController;
        this.redissonClient = redissonClient;
    }

    @Test
    public void when_getTagById_thenShould_setStatusOK() {
        Assertions.assertEquals(tagController.get(EXISTING_TAG_ID).getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void when_getTagById_thenShould_throwException() {
        Assertions.assertThrows(TagNotFoundException.class, () -> tagController.get(NOT_EXISTING_TAG_ID));
    }

    @Test
    public void when_getAllTags_thenShould_returnEntities() {
        Assertions.assertEquals(tagController.getAll().getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void when_addTag_thenShould_returnEntity() {
        Assertions.assertEquals(5, tagController.getAll().getBody().size());
        Assertions.assertEquals(tagController.add(TagDto.builder().name(NOT_EXISTING_TAG_NAME).build()).getStatusCode(),
                HttpStatus.CREATED);
        Assertions.assertEquals(6, tagController.getAll().getBody().size());
    }

    @Test
    public void when_addTag_thenShould_throwException() {
        Assertions.assertEquals(5, tagController.getAll().getBody().size());

        TagDto existingTag = tagController.get(EXISTING_TAG_ID).getBody();
        Assertions.assertThrows(TagNotUniqueNameException.class, () -> tagController.add(existingTag));

        Assertions.assertEquals(5, tagController.getAll().getBody().size());
    }

    @Test
    public void when_deleteTagById_thenShould_returnNothing() {
        Assertions.assertEquals(tagController.delete(EXISTING_TAG_ID).getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    public void when_deleteTagById_thenShould_throwException() {
        Assertions.assertThrows(TagNotFoundException.class, () -> tagController.delete(NOT_EXISTING_TAG_ID));
    }

}
