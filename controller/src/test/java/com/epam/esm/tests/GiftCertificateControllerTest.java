package com.epam.esm.tests;

import com.epam.esm.controller.GiftCertificateController;
import com.epam.esm.controller.TagController;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.GiftCertificateNotFoundException;
import com.epam.esm.exception.GiftCertificateNotUniqueNameException;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Set;
import java.util.stream.Collectors;

@Transactional
public class GiftCertificateControllerTest extends AbstractTestContainer {
    private final GiftCertificateController giftCertificateController;
    private final TagController tagController;
    private final RedissonClient redissonClient;
    private static final String NOT_EXISTING_TAG_NAME = "tagName";
    private static final String GIFT_CERTIFICATE_NAME = "giftCertificateName";
    private static final String DESCRIPTION = "description";
    private static final Double PRICE = 1.00;
    private static final Integer DURATION = 10;
    private static final Long EXISTING_TAG_ID = 1L;
    private static final Long EXISTING_GIFT_CERTIFICATE_ID = 1L;
    private static final Long NOT_EXISTING_GIFT_CERTIFICATE_ID = 100L;
    private static final Integer TWENTY_SECOND = 20000;

    @Autowired
    public GiftCertificateControllerTest(GiftCertificateController giftCertificateController,
                                         TagController tagController, RedissonClient redissonClient) {
        this.giftCertificateController = giftCertificateController;
        this.tagController = tagController;
        this.redissonClient = redissonClient;
    }

    @AfterEach
    public void clear() {
        redissonClient.getKeys().flushdb();
    }

    @Test
    public void when_getGiftCertificateById_thenShould_returnEntity() {
        Assertions.assertEquals(giftCertificateController
                .get(EXISTING_GIFT_CERTIFICATE_ID).getStatusCode(), HttpStatus.OK);
        Assertions.assertDoesNotThrow(() -> giftCertificateController.get(EXISTING_GIFT_CERTIFICATE_ID).getBody());
    }

    @Test
    public void when_getGiftCertificateById_thenShould_throwException() {
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateController.get(NOT_EXISTING_GIFT_CERTIFICATE_ID));
    }

    @Test
    public void when_getAllGiftCertificate_thenShould_returnListOfEntities() {
        Assertions.assertEquals(giftCertificateController.getAll().getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void when_addGiftCertificate_thenShould_returnEntity() throws InterruptedException {
        TagDto existingTag = tagController.get(EXISTING_TAG_ID).getBody();
        TagDto newTag = TagDto.builder().name(NOT_EXISTING_TAG_NAME).build();

        Assertions.assertEquals(2, giftCertificateController.getAll().getBody().size());
        Assertions.assertEquals(5, tagController.getAll().getBody().size());

        Assertions.assertEquals(giftCertificateController.add(GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .duration(DURATION)
                .description(DESCRIPTION)
                .tags(Set.of(existingTag, newTag))
                .build()).getStatusCode(), HttpStatus.CREATED);

        Assertions.assertEquals(3, giftCertificateController.getAll().getBody().size());
        Thread.sleep(TWENTY_SECOND);
        Assertions.assertEquals(6, tagController.getAll().getBody().size());
    }

    @Test
    public void when_addGiftCertificate_thenShould_throwException() {
        Assertions.assertEquals(2, giftCertificateController.getAll().getBody().size());
        Assertions.assertEquals(5, tagController.getAll().getBody().size());

        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateController.add(GiftCertificateDto.builder()
                        .name(giftCertificateController.get(EXISTING_GIFT_CERTIFICATE_ID).getBody().getName())
                        .price(PRICE)
                        .duration(DURATION)
                        .description(DESCRIPTION)
                        .tags(Set.of())
                        .build()));

        Assertions.assertEquals(2, giftCertificateController.getAll().getBody().size());
        Assertions.assertEquals(5, tagController.getAll().getBody().size());
    }

    @Test
    public void when_updateGiftCertificate_thenShould_returnEntity() throws InterruptedException {
        TagDto existingTag = tagController.get(EXISTING_GIFT_CERTIFICATE_ID).getBody();
        TagDto newTag = TagDto.builder().name(NOT_EXISTING_TAG_NAME).build();

        Assertions.assertEquals(5, tagController.getAll().getBody().size());

        GiftCertificateDto existing = giftCertificateController.get(EXISTING_GIFT_CERTIFICATE_ID).getBody();
        GiftCertificateDto forUpdate = GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .duration(DURATION)
                .description(DESCRIPTION)
                .tags(Set.of(existingTag, newTag))
                .build();

        Assertions.assertNotEquals(existing.getName(), forUpdate.getName());
        Assertions.assertNotEquals(existing.getPrice(), forUpdate.getPrice());
        Assertions.assertNotEquals(existing.getDuration(), forUpdate.getDuration());
        Assertions.assertNotEquals(existing.getDescription(), forUpdate.getDescription());
        Assertions.assertNotEquals(existing.getTags().size(), forUpdate.getTags().size());

        ResponseEntity<GiftCertificateDto> responseEntity = giftCertificateController
                .update(EXISTING_GIFT_CERTIFICATE_ID, forUpdate);
        Assertions.assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        GiftCertificateDto updated = responseEntity.getBody();

        Assertions.assertEquals(forUpdate.getName(), updated.getName());
        Assertions.assertEquals(forUpdate.getPrice(), updated.getPrice());
        Assertions.assertEquals(forUpdate.getDuration(), updated.getDuration());
        Assertions.assertEquals(forUpdate.getDescription(), updated.getDescription());
        Assertions.assertTrue(forUpdate.getTags().stream()
                .map(TagDto::getName)
                .allMatch(name -> updated.getTags().stream()
                        .map(TagDto::getName)
                        .collect(Collectors.toSet())
                        .contains(name)));

        Thread.sleep(TWENTY_SECOND);
        Assertions.assertEquals(6, tagController.getAll().getBody().size());
    }

    @Test
    public void when_updateGiftCertificate_thenShould_throwException() {
        Assertions.assertEquals(5, tagController.getAll().getBody().size());

        GiftCertificateDto existing = giftCertificateController.get(EXISTING_GIFT_CERTIFICATE_ID).getBody();
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateController.update(EXISTING_GIFT_CERTIFICATE_ID, existing));
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () ->  giftCertificateController.get(NOT_EXISTING_GIFT_CERTIFICATE_ID));

        Assertions.assertEquals(5, tagController.getAll().getBody().size());
    }

    @Test
    public void when_deleteGiftCertificateById_thenShould_returnNothing() {
        Assertions.assertEquals(giftCertificateController.delete(EXISTING_GIFT_CERTIFICATE_ID).getStatusCode(),
                HttpStatus.NO_CONTENT);
    }

    @Test
    public void when_deleteGiftCertificateById_thenShould_throwException() {
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateController.delete(NOT_EXISTING_GIFT_CERTIFICATE_ID));
    }
}
