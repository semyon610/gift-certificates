package com.epam.esm.tests;

import com.epam.esm.initializers.KafkaTestContainerInitializer;
import com.epam.esm.initializers.PostgresqlTestContainerInitializer;
import com.epam.esm.initializers.RedisTestContainerInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {PostgresqlTestContainerInitializer.class, RedisTestContainerInitializer.class,
        KafkaTestContainerInitializer.class})
public class AbstractTestContainer {

}
