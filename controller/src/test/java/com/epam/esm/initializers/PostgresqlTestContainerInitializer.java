package com.epam.esm.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;
import org.testcontainers.containers.PostgreSQLContainer;

public class PostgresqlTestContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public static final String DOCKER_IMAGE = "postgres:15.2";
    public static final String DB_NAME = "ces_test8";
    public static final String USER = "postgres";
    public static final String PASSWORD = "postgres";
    private static final int POSTGRES_PORT = 5432;
    public static final String INIT_TEST_DB_SCRIPT_SQL = "init_test_db_script.sql";
    public static final PostgreSQLContainer<?> POSTGRE_CONTAINER;

    static {
        POSTGRE_CONTAINER = new PostgreSQLContainer<>(DOCKER_IMAGE)
                .withDatabaseName(DB_NAME)
                .withUsername(USER)
                .withPassword(PASSWORD)
                .withInitScript(INIT_TEST_DB_SCRIPT_SQL);
        POSTGRE_CONTAINER.start();
    }

    private static String getHost() {
        return POSTGRE_CONTAINER.getHost();
    }

    private static int getPort() {
        return POSTGRE_CONTAINER.getMappedPort(POSTGRES_PORT);
    }

    private static String getUrl() {
        String url = "jdbc:postgresql:" +
                UriComponentsBuilder.newInstance().host(getHost()).port(getPort()).path(DB_NAME).toUriString() + "?stringtype=unspecified";
        return url;
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of("spring.datasource.url=" + getUrl(),
                "spring.datasource.username:" + POSTGRE_CONTAINER.getUsername(),
                "spring.datasource.password:" + POSTGRE_CONTAINER.getPassword(),
                "spring.profiles.active:test",
                "spring.liquibase.enabled=false",
                "server.port=8080").applyTo(applicationContext.getEnvironment());
    }
}
