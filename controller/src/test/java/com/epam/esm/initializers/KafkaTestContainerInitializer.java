package com.epam.esm.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;

public class KafkaTestContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final String ZOOKEEPER_TIMEOUT = "5500";
    public static final KafkaContainer KAFKA_CONTAINER;

    static {
        KAFKA_CONTAINER = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));
        KAFKA_CONTAINER.addEnv("KAFKA_ZOOKEEPER_SESSION_TIMEOUT_MS", ZOOKEEPER_TIMEOUT);
        KAFKA_CONTAINER.addEnv("KAFKA_ZOOKEEPER_CONNECTION_TIMEOUT_MS", ZOOKEEPER_TIMEOUT);
        KAFKA_CONTAINER.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of("kafka.bootstrap.server=" + KAFKA_CONTAINER.getBootstrapServers())
                .applyTo(applicationContext.getEnvironment());
    }
}
