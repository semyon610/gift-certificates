package com.epam.esm.initializers;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

public class RedisTestContainerInitializer  implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public static final Integer REDIS_PORT = 6379;
    public static final GenericContainer<?> REDIS_CONTAINER;

    static {
        REDIS_CONTAINER = new GenericContainer(DockerImageName.parse("redis:6.2.7"))
                .withExposedPorts(REDIS_PORT);
        REDIS_CONTAINER.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of("redis.address=" + getRedisAddress(),
                "spring.profiles.active:test",
                "spring.liquibase.enabled=false").applyTo(applicationContext.getEnvironment());
    }

    private static String getRedisAddress() {
        String address = REDIS_CONTAINER.getHost();
        Integer port = REDIS_CONTAINER.getMappedPort(REDIS_PORT);
        return "redis://" + address + ":" + port;
    }
}
