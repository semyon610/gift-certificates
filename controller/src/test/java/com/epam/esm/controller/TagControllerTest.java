package com.epam.esm.controller;

import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.TagNotFoundException;
import com.epam.esm.exception.TagNotUniqueNameException;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class TagControllerTest {
    @Mock
    private TagDto tagDto;
    @Mock
    private TagService tagService;
    @InjectMocks
    private TagController tagController;
    private static final Long TAG_ID = 1L;

    @Test
    void when_getTag_thenShould_returnEntity() {
        Mockito.when(tagService.get(TAG_ID)).thenReturn(tagDto);
        Assertions.assertEquals(new ResponseEntity<>(tagDto, HttpStatus.OK), tagController.get(TAG_ID));
        Mockito.verify(tagService).get(TAG_ID);
    }

    @Test
    void when_getTag_thenShould_throwException() {
        Mockito.when(tagService.get(TAG_ID)).thenThrow(new TagNotFoundException());
        Assertions.assertThrows(TagNotFoundException.class, () -> tagController.get(TAG_ID));
        Mockito.verify(tagService).get(TAG_ID);
    }

    @Test
    void when_getAllTags_thenShould_returnListOfEntities() {
        Mockito.when(tagService.getAll()).thenReturn(List.of(tagDto));
        Assertions.assertEquals(new ResponseEntity<>(List.of(tagDto), HttpStatus.OK), tagController.getAll());
        Mockito.verify(tagService).getAll();
    }

    @Test
    void when_addTag_thenShould_returnSavedEntity() {
        Mockito.when(tagService.add(tagDto)).thenReturn(tagDto);
        Assertions.assertEquals(new ResponseEntity<>(tagDto, HttpStatus.CREATED), tagController.add(tagDto));
        Mockito.verify(tagService).add(Mockito.any(TagDto.class));
    }

    @Test
    void when_addTagWithExistingName_thenShould_throwException() {
        Mockito.when(tagService.add(tagDto)).thenThrow(new TagNotUniqueNameException());
        Assertions.assertThrows(TagNotUniqueNameException.class, () -> tagController.add(tagDto));
        Mockito.verify(tagService).add(Mockito.any(TagDto.class));
    }

    @Test
    void when_deleteTag_thenShould_returnNothing() {
        Mockito.doNothing().when(tagService).delete(TAG_ID);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT), tagController.delete(TAG_ID));
        Mockito.verify(tagService).delete(Mockito.anyLong());
    }

    @Test
    void when_deleteTag_thenShould_throwException() {
        Mockito.doThrow(new TagNotFoundException()).when(tagService).delete(TAG_ID);
        Assertions.assertThrows(TagNotFoundException.class, () -> tagController.delete(TAG_ID));
        Mockito.verify(tagService).delete(Mockito.anyLong());
    }
}