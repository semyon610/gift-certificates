package com.epam.esm.controller;

import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.exception.GiftCertificateNotFoundException;
import com.epam.esm.exception.GiftCertificateNotUniqueNameException;
import com.epam.esm.service.GiftCertificateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class GiftCertificateControllerTest {
    @Mock
    private GiftCertificateDto giftCertificateDto;
    @Mock
    private GiftCertificateService giftCertificateService;
    @InjectMocks
    private GiftCertificateController giftCertificateController;
    private static final Long GIFT_CERTIFICATE_ID = 1L;

    @Test
    void when_getGiftCertificate_thenShould_returnEntity() {
        Mockito.when(giftCertificateService.get(GIFT_CERTIFICATE_ID)).thenReturn(giftCertificateDto);
        Assertions.assertEquals(new ResponseEntity<>(giftCertificateDto, HttpStatus.OK),
                giftCertificateController.get(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateService).get(Mockito.anyLong());
    }

    @Test
    void when_getGiftCertificate_thenShould_throwException() {
        Mockito.when(giftCertificateService.get(GIFT_CERTIFICATE_ID))
                .thenThrow(new GiftCertificateNotFoundException());
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateController.get(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateService).get(Mockito.anyLong());
    }

    @Test
    void when_getAllGiftCertificates_thenShould_returnListOfEntities() {
        Mockito.when(giftCertificateService.getAll()).thenReturn(List.of(giftCertificateDto));
        Assertions.assertEquals(new ResponseEntity<>(List.of(giftCertificateDto), HttpStatus.OK),
                giftCertificateController.getAll());
        Mockito.verify(giftCertificateService).getAll();
    }

    @Test
    void when_addGiftCertificate_thenShould_returnSavedEntity() {
        Mockito.when(giftCertificateService.add(giftCertificateDto)).thenReturn(giftCertificateDto);
        Assertions.assertEquals(new ResponseEntity<>(giftCertificateDto, HttpStatus.CREATED),
                giftCertificateController.add(giftCertificateDto));
        Mockito.verify(giftCertificateService).add(Mockito.any(GiftCertificateDto.class));
    }

    @Test
    void when_addGiftCertificateWithExistingName_thenShould_throwException() {
        Mockito.when(giftCertificateService.add(giftCertificateDto))
                .thenThrow(new GiftCertificateNotUniqueNameException());
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateController.add(giftCertificateDto));
        Mockito.verify(giftCertificateService).add(Mockito.any(GiftCertificateDto.class));
    }

    @Test
    void when_updateGiftCertificate_thenShould_returnEntity() {
        Mockito.when(giftCertificateService.update(GIFT_CERTIFICATE_ID, giftCertificateDto))
                .thenReturn(giftCertificateDto);
        Assertions.assertEquals(new ResponseEntity<>(giftCertificateDto, HttpStatus.OK),
                giftCertificateController.update(GIFT_CERTIFICATE_ID, giftCertificateDto));
        Mockito.verify(giftCertificateService).update(Mockito.anyLong(), Mockito.any(GiftCertificateDto.class));
    }

    @Test
    void when_updateGiftCertificate_thenShould_returnException() {
        Mockito.when(giftCertificateService.update(GIFT_CERTIFICATE_ID, giftCertificateDto))
                .thenThrow(new GiftCertificateNotFoundException());
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateController.update(GIFT_CERTIFICATE_ID, giftCertificateDto));
        Mockito.verify(giftCertificateService).update(Mockito.anyLong(), Mockito.any(GiftCertificateDto.class));
    }

    @Test
    void when_updateGiftCertificateWithExistingName_thenShould_returnException() {
        Mockito.when(giftCertificateService.update(GIFT_CERTIFICATE_ID, giftCertificateDto))
                .thenThrow(new GiftCertificateNotUniqueNameException());
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateController.update(GIFT_CERTIFICATE_ID, giftCertificateDto));
        Mockito.verify(giftCertificateService).update(Mockito.anyLong(), Mockito.any(GiftCertificateDto.class));
    }

    @Test
    void when_deleteGiftCertificate_thenShould_returnNothing() {
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.NO_CONTENT),
                giftCertificateController.delete(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateService).delete(Mockito.anyLong());
    }

    @Test
    void when_deleteGiftCertificate_thenShould_throwException() {
        Mockito.doThrow(new GiftCertificateNotFoundException()).when(giftCertificateService)
                .delete(GIFT_CERTIFICATE_ID);
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateController.delete(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateService).delete(Mockito.anyLong());
    }
}