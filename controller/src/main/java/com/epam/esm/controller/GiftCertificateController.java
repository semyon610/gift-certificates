package com.epam.esm.controller;

import com.epam.esm.dao.entity.GiftCertificate;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.service.GiftCertificateService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.EqualIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The {@code GiftCertificateController} class is an API endpoint.
 * that provides crud operations.
 * <p>
 * The controller is accessible by "/gift_certificates".
 * The response content type is application/json.
 * </p>
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/gift_certificates")
@Validated
public class GiftCertificateController {
    /**
     * Injection of a dependency implementing a class {@link GiftCertificateService}.
     */
    private final GiftCertificateService giftCertificateService;

    /**
     * Returns the gift certificate by the specific identifier.
     * <p>
     * The method is accessible by "/{id}".
     * Variable id is a natural number.
     * If there is no gift certificate with the specified id response gets status 404 - Not Found.
     * The default response status is 200 - OK.
     * </p>
     *
     * @param id Identifier of the requested gift certificate from the request URI.
     * @return {@link ResponseEntity} with found gift certificate.
     */
    @Operation(summary = "Get a gift certificate by its id")
    @GetMapping("/{id}")
    public ResponseEntity<GiftCertificateDto> get(@PathVariable @Positive Long id) {
        return new ResponseEntity<>(giftCertificateService.get(id), HttpStatus.OK);
    }

    /**
     * Returns all gift certificates.
     * <p>
     * The method is accessible by "/all".
     * The default response status is 200 - OK.
     * </p>
     *
     * @return {@link ResponseEntity} with a list of found gift certificates.
     */
    @Operation(summary = "Get all gift certificates")
    @GetMapping("/all")
    public ResponseEntity<List<GiftCertificateDto>> getAll() {
        return new ResponseEntity<>(giftCertificateService.getAll(), HttpStatus.OK);
    }

    /**
     * Returns gift certificates by parameters.
     * If there is no parameters method returns all gift certificates.
     * <p>
     * Accepts optional request parameters {@code tag_name}, {@code gift_cert_name}, {@code description}.
     * <p>
     * <p>
     * The method is accessible by "/param".
     * The default response status is 200 - OK.
     * </p>
     *
     * @param messageSpecification {@link Specification} that accepts request parameters.
     * @return {@link ResponseEntity} with a list of found gift certificates.
     */
    @Operation(summary = "Get gift certificates by parameters")
    @GetMapping(path = "/param")
    public ResponseEntity<List<GiftCertificateDto>> getMessageFilters(
            @Join(path = "tags", alias = "t")
            @And({
                    @Spec(path = "t.name", params = "tag_name", spec = EqualIgnoreCase.class),
                    @Spec(path = "name", params = "gift_cert_name", spec = LikeIgnoreCase.class),
                    @Spec(path = "description", params = "description", spec = LikeIgnoreCase.class)})
            Specification<GiftCertificate> messageSpecification) {
        return new ResponseEntity<>(giftCertificateService.findAll(messageSpecification), HttpStatus.OK);
    }

    /**
     * Saves the gift certificate and returns it.
     * <p>
     * The method is accessible by "/".
     * If the gift certificate name already exists response gets status 409 - Conflict.
     * The default response status is 200 - OK.
     * </p>
     *
     * @param giftCertificateDto The gift certificate that is saved in the storage.
     * @return {@link ResponseEntity} with a saved gift certificate.
     */
    @Operation(summary = "Add a gift certificate")
    @PostMapping("/")
    public ResponseEntity<GiftCertificateDto> add(@RequestBody @Valid GiftCertificateDto giftCertificateDto) {
        return new ResponseEntity<>(giftCertificateService.add(giftCertificateDto), HttpStatus.CREATED);
    }

    /**
     * Update the gift certificate and returns it.
     * All parameters of the request body are optional.
     * <p>
     * The method is accessible by "/{id}".
     * Variable id is a natural number.
     * If the gift certificate with such id does not exist response gets status 404 - Not found.
     * If the gift certificate name already exists response gets status 409 - Conflict.
     * The default response status is 200 - OK.
     * </p>
     *
     * @param id                 Identifier of the requested gift certificate from the request URI.
     * @param giftCertificateDto The gift certificate that is saved in the storage.
     * @return {@link ResponseEntity} with a saved gift certificate.
     */
    @Operation(summary = "Update a gift certificate by its id")
    @PutMapping("/{id}")
    public ResponseEntity<GiftCertificateDto> update(@PathVariable @Positive Long id,
                                                     @RequestBody @Valid GiftCertificateDto giftCertificateDto) {
        return new ResponseEntity<>(giftCertificateService.update(id, giftCertificateDto), HttpStatus.OK);
    }

    /**
     * Delete the gift certificate.
     * <p>
     * The method is accessible by "/{id}".
     * Variable id is a natural number.
     * If the gift certificate does not exist response gets status 404 - Not found.
     * The default response status is 204 - No content.
     * </p>
     *
     * @param id Identifier of the requested gift certificate from the request URI.
     * @return {@link ResponseEntity} with a saved gift certificate.
     */
    @Operation(summary = "Delete a gift certificate by its id")
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable @Positive Long id) {
        giftCertificateService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
