package com.epam.esm.controller;

import com.epam.esm.dto.TagDto;
import com.epam.esm.service.TagService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The {@code TagController} class is an API endpoint.
 * that provides crd operations.
 * <p>
 * The controller is accessible by "/tags".
 * The response content type is application/json.
 * </p>
 */
@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping("/tags")
public class TagController {
    /**
     * Injection of a dependency implementing a class {@link TagService}.
     */
    private final TagService tagService;

    /**
     * Returns a tag by the specific identifier.
     * <p>
     * The method is accessible by "/{id}".
     * Variable id is a natural number.
     * If there is no tag with the specified id response gets status 404 - Not Found.
     * The default response status is 200 - OK.
     * </p>
     * @param id Identifier of the requested tag from the request URI.
     * @return {@link ResponseEntity} with found tag.
     */
    @Operation(summary = "Get a tag by its id")
    @GetMapping("/{id}")
    public ResponseEntity<TagDto> get(@PathVariable @Positive Long id) {
        return new ResponseEntity<>(tagService.get(id), HttpStatus.OK);
    }

    /**
     * Returns all tags.
     * <p>
     * The method is accessible by "/all".
     * The default response status is 200 - OK.
     * </p>
     * @return {@link ResponseEntity} with a list of found tags.
     */
    @Operation(summary = "Get all tags")
    @GetMapping("/all")
    public ResponseEntity<List<TagDto>> getAll() {
        return new ResponseEntity<>(tagService.getAll(), HttpStatus.OK);
    }

    /**
     * Saves the tag and returns it.
     * <p>
     * The method is accessible by "/".
     * If the tag name already exists response gets status 409 - Conflict.
     * The default response status is 200 - OK.
     * </p>
     *
     * @param tagDto The tag that is saved in the storage.
     * @return {@link ResponseEntity} with a saved tag.
     */
    @Operation(summary = "Add a tag")
    @PostMapping("/")
    public ResponseEntity<TagDto> add(@RequestBody @Valid TagDto tagDto) {
        return new ResponseEntity<>(tagService.add(tagDto), HttpStatus.CREATED);
    }

    /**
     * Delete the tag.
     * <p>
     * The method is accessible by "/{id}".
     * Variable id is a natural number.
     * If the tag does not exist response gets status 404 - Not found.
     * The default response status is 204 - No content.
     * </p>
     *
     * @param id Identifier of the requested gift certificate from the request URI.
     * @return {@link ResponseEntity} with a saved tag.
     */
    @Operation(summary = "Delete a tag by its id")
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable @Positive Long id) {
        tagService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
