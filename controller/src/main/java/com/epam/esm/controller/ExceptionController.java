package com.epam.esm.controller;

import com.epam.esm.exception.*;
import com.epam.esm.handler.ApiError;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@code ExceptionController} class handles exceptions in certain handler methods.
 */
@RestControllerAdvice
public class ExceptionController {

    /**
     * The exception {@link GiftCertificateNotFoundException} is thrown at the service level.
     * <p>
     * This is a custom exception that is called if the gift certificate was not found in the repository.
     * </p>
     *
     * @param exception {@code GiftCertificateNotFoundException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(GiftCertificateNotFoundException.class)
    public ResponseEntity<ApiError> giftCertificateNotFoundException(GiftCertificateNotFoundException exception) {
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()));
    }

    /**
     * The exception {@link TagNotFoundException} is thrown at the service level.
     * <p>
     * This is a custom exception that is called if the tag was not found in the repository.
     * </p>
     *
     * @param exception {@code TagNotFoundException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(TagNotFoundException.class)
    public ResponseEntity<ApiError> tagNotFoundException(TagNotFoundException exception) {
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()));
    }

    /**
     * The exception {@link GiftCertificateNotUniqueNameException} is thrown at the service level.
     * <p>
     * This is a custom exception that is called if the gift certificate name is not unique.
     * </p>
     *
     * @param exception {@code GiftCertificateNotUniqueNameException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(GiftCertificateNotUniqueNameException.class)
    public ResponseEntity<ApiError> giftCertificateNotUniqueNameException(GiftCertificateNotUniqueNameException exception) {
        return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, exception.getMessage()));
    }

    /**
     * The exception {@link TagNotUniqueNameException} is thrown at the service level.
     * <p>
     * This is a custom exception that is called if the tag name is not unique.
     * </p>
     *
     * @param exception {@code TagNotUniqueNameException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(TagNotUniqueNameException.class)
    public ResponseEntity<ApiError> tagNotUniqueNameException(TagNotUniqueNameException exception) {
        return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, exception.getMessage()));
    }

    /**
     * The exception {@link GiftCertificateWrongField} is thrown at the service level.
     * <p>
     * This is a custom exception that is called if all fields have been filled in.
     * </p>
     *
     * @param exception {@code GiftCertificateWrongField}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(GiftCertificateWrongField.class)
    public ResponseEntity<ApiError> giftCertificateWrongField(GiftCertificateWrongField exception) {
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, exception.getMessage()));
    }

    /**
     * The exception {@link ConstraintViolationException} is thrown when data validation failed.
     *
     * @param exception {@code ConstraintViolationException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiError> constraintViolationException(ConstraintViolationException exception) {
        Map<String, String> violations = new HashMap<>();
        exception.getConstraintViolations()
                .forEach(error -> violations.put(error.getPropertyPath().toString(), error.getMessage()));
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, violations));
    }

    /**
     * Exception {@link MethodArgumentNotValidException} to be thrown when validation on an argument
     * annotated with @Valid fails.
     *
     * @param exception{@code MethodArgumentNotValidException}
     * @return {@link ResponseEntity} with message {@link ApiError}
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        Map<String, String> violations = new HashMap<>();
        exception.getBindingResult().getFieldErrors()
                .forEach(error -> violations.put(error.getField(), error.getDefaultMessage()));
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, violations));
    }

    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }
}
