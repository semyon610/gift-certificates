package com.epam.esm.handler;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The {@code ApiError} class represents custom exceptions for the web layer.
 * Contains a description of the error: status, message and time
 * </p>
 */
@Data
public class ApiError implements Serializable {

    private static final long serialVersionUID = -1503262266388033412L;
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime timestamp;
    private Object message;


    private ApiError() {
        this.timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status, Object message) {
        this();
        this.status = status;
        this.message = message;
    }

}
