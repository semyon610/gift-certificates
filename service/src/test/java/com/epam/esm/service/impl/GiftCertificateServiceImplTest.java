package com.epam.esm.service.impl;

import com.epam.esm.dao.entity.GiftCertificate;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.GiftCertificateNotFoundException;
import com.epam.esm.exception.GiftCertificateNotUniqueNameException;
import com.epam.esm.exception.GiftCertificateWrongField;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.service.TagService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.redisson.api.RedissonClient;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
class GiftCertificateServiceImplTest {
    @Mock
    private GiftCertificateRepository giftCertificateRepository;
    @Mock
    private TagService tagService;
    @Mock
    private GiftCertificateMapper giftCertificateMapper;
    @Mock
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Mock
    private RedissonClient redissonClient;
    @Mock
    private GiftCertificate giftCertificate;
    @Mock
    private GiftCertificateDto giftCertificateDto;
    @Mock
    private TagDto tagDto;
    @Mock
    private Specification<GiftCertificate> specification;
    @InjectMocks
    private GiftCertificateServiceImpl giftCertificateService;
    private static final Long GIFT_CERTIFICATE_ID = 1L;
    private static final String GIFT_CERTIFICATE_NAME = "giftCertificateName";
    private static final String DESCRIPTION = "description";
    private static final Double PRICE = 1.00;
    private static final Integer DURATION = 10;

    @Test
    void when_getGiftCertificateById_thenShould_returnEntity() {
        Mockito.when(giftCertificateRepository.findById(GIFT_CERTIFICATE_ID)).thenReturn(Optional.of(giftCertificate));
        Mockito.when(giftCertificateMapper.toDto(giftCertificate)).thenReturn(giftCertificateDto);
        Mockito.when(tagService.findAllByGiftCertificateId(GIFT_CERTIFICATE_ID)).thenReturn(Set.of(tagDto));
        Assertions.assertEquals(giftCertificateDto, giftCertificateService.get(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateRepository).findById(Mockito.anyLong());
        Mockito.verify(giftCertificateMapper).toDto(Mockito.any(GiftCertificate.class));
        Mockito.verify(tagService).findAllByGiftCertificateId(Mockito.anyLong());
    }

    @Test
    void when_getGiftCertificateById_thenShould_throwException() {
        Mockito.when(giftCertificateRepository.findById(GIFT_CERTIFICATE_ID)).thenReturn(Optional.empty());
        Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> giftCertificateService.get(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateRepository).findById(Mockito.anyLong());
    }

    @Test
    void when_getAllGiftCertificates_thenShould_returnListOfEntities() {
        List<GiftCertificate> list = List.of(giftCertificate);
        List<GiftCertificateDto> listDto = List.of(giftCertificateDto);

        Mockito.when(giftCertificateRepository.findAll()).thenReturn(list);
        Mockito.when(giftCertificateMapper.toDto(list)).thenReturn(listDto);
        Assertions.assertEquals(listDto, giftCertificateService.getAll());
        Mockito.verify(giftCertificateRepository).findAll();
        Mockito.verify(giftCertificateMapper).toDto(Mockito.anyList());
    }

    @Test
    void when_getAllGiftCertificates_thenShould_returnEmptyList() {
        List<GiftCertificate> list = List.of();
        List<GiftCertificateDto> listDto = List.of();

        Mockito.when(giftCertificateRepository.findAll()).thenReturn(list);
        Mockito.when(giftCertificateMapper.toDto(list)).thenReturn(listDto);
        Assertions.assertEquals(listDto, giftCertificateService.getAll());
        Mockito.verify(giftCertificateRepository).findAll();
        Mockito.verify(giftCertificateMapper).toDto(Mockito.anyList());
    }

    @Test
    void when_findAllGiftCertificates_thenShould_returnListOfEntities() {
        List<GiftCertificate> list = List.of(giftCertificate);
        List<GiftCertificateDto> listDto = List.of(giftCertificateDto);

        Mockito.when(giftCertificateRepository.findAll(specification)).thenReturn(list);
        Mockito.when(giftCertificateMapper.toDto(list)).thenReturn(listDto);
        Assertions.assertEquals(listDto, giftCertificateService.findAll(specification));
        Mockito.verify(giftCertificateRepository).findAll(Mockito.any());
        Mockito.verify(giftCertificateMapper).toDto(Mockito.anyList());
    }

    @Test
    void when_findAllGiftCertificates_thenShould_throwException() {
        List<GiftCertificate> list = List.of();
        List<GiftCertificateDto> listDto = List.of();

        Mockito.when(giftCertificateRepository.findAll(specification)).thenReturn(list);
        Mockito.when(giftCertificateMapper.toDto(list)).thenReturn(listDto);
        Assertions.assertEquals(listDto, giftCertificateService.findAll(specification));
        Mockito.verify(giftCertificateRepository).findAll(Mockito.any());
        Mockito.verify(giftCertificateMapper).toDto(Mockito.anyList());
    }

    @Test
    void when_saveGiftCertificate_thenShould_returnSavedEntity() {
        GiftCertificateDto dto = GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .description(DESCRIPTION)
                .duration(DURATION)
                .tags(Set.of(tagDto))
                .build();
        Mockito.when(giftCertificateRepository.existsByNameIgnoreCase(Mockito.anyString())).thenReturn(Boolean.FALSE);
        Mockito.when(giftCertificateMapper.toEntity(dto)).thenReturn(giftCertificate);
        Mockito.when(giftCertificateRepository.save(giftCertificate)).thenReturn(giftCertificate);
        Mockito.when(giftCertificateMapper.toDto(giftCertificate)).thenReturn(dto);
        Assertions.assertEquals(dto, giftCertificateService.add(dto));
        Mockito.verify(giftCertificateRepository).existsByNameIgnoreCase(Mockito.anyString());
        Mockito.verify(giftCertificateMapper).toEntity(Mockito.any(GiftCertificateDto.class));
        Mockito.verify(giftCertificateRepository).save(Mockito.any(GiftCertificate.class));
        Mockito.verify(giftCertificateMapper).toDto(Mockito.any(GiftCertificate.class));
    }

    @Test
    void when_saveGiftCertificateWithExistingName_thenShould_throwException() {
        GiftCertificateDto dto = GiftCertificateDto.builder()
                .name(GIFT_CERTIFICATE_NAME)
                .price(PRICE)
                .description(DESCRIPTION)
                .duration(DURATION)
                .tags(Set.of(tagDto))
                .build();
        Mockito.when(giftCertificateRepository.existsByNameIgnoreCase(Mockito.any())).thenReturn(Boolean.TRUE);
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class, () -> giftCertificateService.add(dto));
        Mockito.verify(giftCertificateRepository).existsByNameIgnoreCase(Mockito.anyString());
    }

    @Test
    void when_saveGiftCertificateWithEmptyFields_thenShould_throwException() {
        Assertions.assertThrows(GiftCertificateWrongField.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .price(PRICE)
                        .duration(DURATION)
                        .description(DESCRIPTION)
                        .tags(Set.of())
                        .build()));
        Assertions.assertThrows(GiftCertificateWrongField.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .name(GIFT_CERTIFICATE_NAME)
                        .duration(DURATION)
                        .description(DESCRIPTION)
                        .tags(Set.of())
                        .build()));
        Assertions.assertThrows(GiftCertificateWrongField.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .name(GIFT_CERTIFICATE_NAME)
                        .price(PRICE)
                        .description(DESCRIPTION)
                        .tags(Set.of())
                        .build()));
        Assertions.assertThrows(GiftCertificateWrongField.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .name(GIFT_CERTIFICATE_NAME)
                        .price(PRICE)
                        .duration(DURATION)
                        .tags(Set.of())
                        .build()));
        Assertions.assertThrows(GiftCertificateWrongField.class,
                () -> giftCertificateService.add(GiftCertificateDto.builder()
                        .name(GIFT_CERTIFICATE_NAME)
                        .price(PRICE)
                        .duration(DURATION)
                        .description(DESCRIPTION)
                        .build()));
    }

    @Test
    void when_updateGiftCertificate_thenShould_returnUpdatedEntity() {
        Mockito.when(giftCertificateRepository.findById(GIFT_CERTIFICATE_ID)).thenReturn(Optional.of(giftCertificate));
        Mockito.when(giftCertificateMapper.toDto(giftCertificate)).thenReturn(giftCertificateDto);
        Assertions.assertEquals(giftCertificateDto, giftCertificateService.update(GIFT_CERTIFICATE_ID, giftCertificateDto));
        Mockito.verify(giftCertificateRepository).findById(Mockito.anyLong());
        Mockito.verify(giftCertificateMapper).toDto(Mockito.any(GiftCertificate.class));
    }

    @Test
    void when_updateGiftCertificateWithExistingName_thenShould_throwException() {
        Mockito.when(giftCertificateRepository.findById(GIFT_CERTIFICATE_ID)).thenReturn(Optional.of(giftCertificate));
        Mockito.when(giftCertificateDto.getName()).thenReturn(GIFT_CERTIFICATE_NAME);
        Mockito.when(giftCertificateRepository.existsByNameIgnoreCase(GIFT_CERTIFICATE_NAME)).thenReturn(Boolean.TRUE);
        Assertions.assertThrows(GiftCertificateNotUniqueNameException.class,
                () -> giftCertificateService.update(GIFT_CERTIFICATE_ID, giftCertificateDto));
        Mockito.verify(giftCertificateRepository).findById(Mockito.anyLong());
        Mockito.verify(giftCertificateDto, Mockito.times(2)).getName();
        Mockito.verify(giftCertificateRepository).existsByNameIgnoreCase(Mockito.anyString());
    }

    @Test
    void when_updateGiftCertificate_thenShould_throwException() {
        Mockito.when(giftCertificateRepository.findById(GIFT_CERTIFICATE_ID)).thenReturn(Optional.empty());
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateService.update(GIFT_CERTIFICATE_ID, Mockito.any()));
        Mockito.verify(giftCertificateRepository).findById(Mockito.anyLong());
    }

    @Test
    void when_deleteGiftCertificate_thenShould_returnNothing() {
        Mockito.when(giftCertificateRepository.existsById(GIFT_CERTIFICATE_ID)).thenReturn(Boolean.TRUE);
        Assertions.assertDoesNotThrow(() -> giftCertificateService.delete(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateRepository).existsById(Mockito.anyLong());
    }

    @Test
    void when_deleteGiftCertificate_thenShould_throwException() {
        Mockito.when(giftCertificateRepository.existsById(GIFT_CERTIFICATE_ID)).thenReturn(Boolean.FALSE);
        Assertions.assertThrows(GiftCertificateNotFoundException.class,
                () -> giftCertificateService.delete(GIFT_CERTIFICATE_ID));
        Mockito.verify(giftCertificateRepository).existsById(Mockito.anyLong());
    }
}