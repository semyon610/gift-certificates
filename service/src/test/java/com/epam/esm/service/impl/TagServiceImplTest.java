package com.epam.esm.service.impl;

import com.epam.esm.dao.entity.Tag;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.TagNotFoundException;
import com.epam.esm.exception.TagNotUniqueNameException;
import com.epam.esm.mapper.TagMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.redisson.api.RedissonClient;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
class TagServiceImplTest {
    @Mock
    private Tag tag;
    @Mock
    private TagDto tagDto;
    @Mock
    private TagRepository tagRepository;
    @Mock
    private TagMapper tagMapper;
    @Mock
    private KafkaTemplate<String, Object> kafkaTemplate;
    @Mock
    private RedissonClient redissonClient;
    @Mock
    private CacheManager cacheManager;
    @Mock
    private Cache cache;
    @InjectMocks
    private TagServiceImpl tagService;
    private static final Long TAG_ID = 1L;
    private static final Long GIFT_CERTIFICATE_ID = 1L;
    private static final String TAG_NAME = "tagName";


    @Test
    void when_getTagById_thenShould_returnEntity() {
        Mockito.when(tagRepository.findById(TAG_ID)).thenReturn(Optional.of(tag));
        Mockito.when(tagMapper.toDto(tag)).thenReturn(tagDto);
        Assertions.assertEquals(tagDto, tagService.get(TAG_ID));
        Mockito.verify(tagRepository).findById(Mockito.anyLong());
        Mockito.verify(tagMapper).toDto(Mockito.any(Tag.class));
    }

    @Test
    void when_getTagById_thenShould_throwException() {
        Mockito.when(tagRepository.findById(TAG_ID)).thenReturn(Optional.empty());
        Assertions.assertThrows(TagNotFoundException.class, () -> tagService.get(TAG_ID));
        Mockito.verify(tagRepository).findById(Mockito.anyLong());
    }

    @Test
    void when_getAllTags_thenShould_returnListOfEntities() {
        List<Tag> tags = List.of(tag);
        List<TagDto> tagDtoList = List.of(tagDto);

        Mockito.when(tagRepository.findAll()).thenReturn(tags);
        Mockito.when(tagMapper.toDto(tags)).thenReturn(tagDtoList);
        Assertions.assertEquals(tagDtoList, tagService.getAll());
        Mockito.verify(tagRepository).findAll();
        Mockito.verify(tagMapper).toDto(Mockito.anyList());
    }

    @Test
    void when_saveTag_thenShould_returnEntity() {

        Mockito.when(tagMapper.toEntity(tagDto)).thenReturn(tag);
        Mockito.when(tagRepository.save(tag)).thenReturn(tag);
        Mockito.when(tagMapper.toDto(tag)).thenReturn(tagDto);
        Mockito.when(cacheManager.getCache(Mockito.anyString())).thenReturn(cache);
        Mockito.doNothing().when(cache).clear();
        Assertions.assertEquals(tagDto, tagService.add(tagDto));
        Mockito.verify(cache).clear();
        Mockito.verify(cacheManager).getCache(Mockito.anyString());
        Mockito.verify(tagMapper).toEntity(Mockito.any(TagDto.class));
        Mockito.verify(tagRepository).save(Mockito.any(Tag.class));
        Mockito.verify(tagMapper).toDto(Mockito.any(Tag.class));
    }

    @Test
    void when_saveTagWithExistingName_thenShould_throwException() {
        Mockito.when(tagDto.getName()).thenReturn(TAG_NAME);
        Mockito.when(tagRepository.existsByNameIgnoreCase(Mockito.any())).thenReturn(Boolean.TRUE);
        Assertions.assertThrows(TagNotUniqueNameException.class, () -> tagService.add(tagDto));
        Mockito.verify(tagDto).getName();
        Mockito.verify(tagRepository).existsByNameIgnoreCase(Mockito.anyString());
    }

    @Test
    void when_deleteTagById_thenShould_returnNothing() {
        Mockito.when(tagRepository.existsById(TAG_ID)).thenReturn(Boolean.TRUE);
        Mockito.when(cacheManager.getCache(Mockito.anyString())).thenReturn(cache);
        Mockito.doNothing().when(cache).evict(Mockito.anyLong());
        Assertions.assertDoesNotThrow(() -> tagService.delete(TAG_ID));
        Mockito.verify(cache).evict(Mockito.anyLong());
        Mockito.verify(cacheManager).getCache(Mockito.anyString());
        Mockito.verify(tagRepository).existsById(Mockito.anyLong());
    }

    @Test
    void when_deleteTagById_thenShould_throwException() {
        Mockito.when(tagRepository.existsById(TAG_ID)).thenReturn(Boolean.FALSE);
        Assertions.assertThrows(TagNotFoundException.class, () -> tagService.delete(TAG_ID));
        Mockito.verify(tagRepository).existsById(Mockito.anyLong());
    }

    @Test
    void when_findAllTagsByGiftCertificateId_thenShould_returnListOfEntities() {
        Set<Tag> tags = Set.of(tag);
        Set<TagDto> tagDtoList = Set.of(tagDto);

        Mockito.when(tagRepository.findAllByGiftCertificateId(GIFT_CERTIFICATE_ID)).thenReturn(tags);
        Mockito.when(tagMapper.toDto(tags)).thenReturn(tagDtoList);
        Assertions.assertEquals(tagDtoList, tagService.findAllByGiftCertificateId(GIFT_CERTIFICATE_ID));
        Mockito.verify(tagRepository).findAllByGiftCertificateId(Mockito.anyLong());
        Mockito.verify(tagMapper).toDto(Mockito.anySet());
    }
}