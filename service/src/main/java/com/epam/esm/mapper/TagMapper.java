package com.epam.esm.mapper;

import com.epam.esm.dao.entity.Tag;
import com.epam.esm.dto.TagDto;
import com.epam.esm.dto.TagRequestDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;


/**
 * For mapping tags.
 */
@Mapper(componentModel = "spring")
public interface TagMapper {
    TagDto toDto(Tag tag);

    TagRequestDto toRequestDto(Tag tag);

    Tag toEntity(TagDto tagDto);

    Set<TagDto> toDto(Set<Tag> tags);

    List<TagDto> toDto(List<Tag> tags);

    Set<Tag> toEntity(Set<TagDto> tags);
}
