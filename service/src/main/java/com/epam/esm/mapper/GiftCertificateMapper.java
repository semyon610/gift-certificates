package com.epam.esm.mapper;

import com.epam.esm.dao.entity.GiftCertificate;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.GiftCertificateRequestDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * For mapping gift certificates.
 */
@Mapper(componentModel = "spring")
public interface GiftCertificateMapper {

    GiftCertificate toEntity(GiftCertificateDto giftCertificateDto);

    GiftCertificateDto toDto(GiftCertificate giftCertificate);

    GiftCertificateRequestDto toRequestDto(GiftCertificate giftCertificate);

    List<GiftCertificateDto> toDto(List<GiftCertificate> giftCertificate);
}
