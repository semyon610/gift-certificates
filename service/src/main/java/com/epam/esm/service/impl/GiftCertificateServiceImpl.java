package com.epam.esm.service.impl;

import com.epam.esm.dao.entity.GiftCertificate;
import com.epam.esm.dao.repository.GiftCertificateRepository;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.GiftCertificateRequestDto;
import com.epam.esm.dto.TagDto;
import com.epam.esm.exception.GiftCertificateNotFoundException;
import com.epam.esm.exception.GiftCertificateNotUniqueNameException;
import com.epam.esm.exception.GiftCertificateWrongField;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import jakarta.annotation.PostConstruct;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.epam.esm.util.CacheConstants.GIFT_CERTIFICATE_DTO_KEY;
import static com.epam.esm.util.CacheConstants.GIFT_CERTIFICATE_DTO_KEY_ALL;
import static com.epam.esm.util.KafkaConstants.GIFT_CERTIFICATE_TOPIC;
import static com.epam.esm.util.RedissonConstants.REDISSON_GIFT_CERTIFICATE;

/**
 * The class represents service methods for gift certificates with work at the dao level.
 */
@Service
public class GiftCertificateServiceImpl implements GiftCertificateService {
    private final GiftCertificateRepository giftCertificateRepository;
    private final TagService tagService;
    private final GiftCertificateMapper giftCertificateMapper;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final RedissonClient redissonClient;
    private RList<GiftCertificateRequestDto> rList;

    @Autowired
    public GiftCertificateServiceImpl(GiftCertificateRepository giftCertificateRepository,
                                      TagService tagService,
                                      GiftCertificateMapper giftCertificateMapper,
                                      @Qualifier("certKafkaTemplate") KafkaTemplate<String, Object> kafkaTemplate,
                                      RedissonClient redissonClient) {
        this.giftCertificateRepository = giftCertificateRepository;
        this.tagService = tagService;
        this.giftCertificateMapper = giftCertificateMapper;
        this.kafkaTemplate = kafkaTemplate;
        this.redissonClient = redissonClient;
    }

    @PostConstruct
    private void init() {
        rList = redissonClient.getList(REDISSON_GIFT_CERTIFICATE);
    }

    @Override
    public RList<GiftCertificateRequestDto> getrList() {
        return rList;
    }

    /**
     * Get and cache a gift certificate by id.
     * @param id unique id of gift certificate
     * @return {@link  GiftCertificateDto}
     */
    @Cacheable(cacheNames = GIFT_CERTIFICATE_DTO_KEY, key = "#id")
    @Override
    public GiftCertificateDto get(Long id) {
        GiftCertificateDto giftCertificateDto = giftCertificateMapper.toDto(
                giftCertificateRepository.findById(id).orElseThrow(GiftCertificateNotFoundException::new));
        Set<TagDto> tags = tagService.findAllByGiftCertificateId(id);
        giftCertificateDto.setTags(tags);
        return giftCertificateDto;

    }

    /**
     * Get and cache all gift certificates.
     * @return {@link  List<GiftCertificateDto>}
     */
    @Cacheable(cacheNames = GIFT_CERTIFICATE_DTO_KEY_ALL)
    @Override
    public List<GiftCertificateDto> getAll() {
        return giftCertificateMapper.toDto(giftCertificateRepository.findAll());
    }

    /**
     * Get all gift certificates using specification
     * @return {@link  List<GiftCertificateDto>}
     */
    @Override
    public List<GiftCertificateDto> findAll(Specification<GiftCertificate> spec){
        return giftCertificateMapper.toDto(giftCertificateRepository.findAll(spec));
    }

    /**
     * Add a gift certificate to the database.
     * Clears the cache of all certificates because the cache value has been changed
     * For educational purposes: send the gift certificate as {@link GiftCertificateRequestDto}
     * using {@link KafkaTemplate} to listener {@link com.epam.esm.kafka.listener.Listener}
     * where it saves into Redis database as {@link RList}
     *
     * @param giftCertificateDto New gift certificate.
     * @return {@link  GiftCertificateDto}
     */
    @CacheEvict(cacheNames = GIFT_CERTIFICATE_DTO_KEY_ALL, allEntries = true)
    @Transactional
    @Override
    public GiftCertificateDto add(GiftCertificateDto giftCertificateDto) {
        verifyGiftCertificateFields(giftCertificateDto);
        verifyGiftCertificateName(giftCertificateDto.getName());
        GiftCertificate giftCertificate = giftCertificateMapper.toEntity(giftCertificateDto);
        LocalDateTime currentTime = LocalDateTime.now();
        giftCertificate.setCreateDate(currentTime);
        giftCertificate.setLastUpdateDate(currentTime);
        giftCertificate.setTags(tagService.saveNotExistingTags(giftCertificateDto.getTags()));
        GiftCertificate savedGiftCertificate = giftCertificateRepository.save(giftCertificate);
        GiftCertificateDto dto = giftCertificateMapper.toDto(savedGiftCertificate);
        kafkaTemplate.send(GIFT_CERTIFICATE_TOPIC, giftCertificateMapper.toRequestDto(savedGiftCertificate));
        return dto;
    }

    /**
     * Update the gift certificate from the database.
     * Updates the gift certificate cache by id.
     * Clears the cache of all certificates because the cache value has been changed.
     * For educational purposes: send the gift certificate as {@link GiftCertificateRequestDto}
     * using {@link KafkaTemplate} to listener {@link com.epam.esm.kafka.listener.Listener}
     * where it saves into Redis database as {@link RList}
     *
     * @param giftCertificateDto Gift certificate with new fields.
     * @return {@link  GiftCertificateDto} Updated gift certificate.
     */
    @Caching(
            put = {
                    @CachePut(cacheNames = GIFT_CERTIFICATE_DTO_KEY, key = "#id")
            },
            evict = {
                    @CacheEvict(cacheNames = GIFT_CERTIFICATE_DTO_KEY_ALL, allEntries = true)
            }
    )
    @Transactional
    @Override
    public GiftCertificateDto update(Long id, GiftCertificateDto giftCertificateDto) {
        GiftCertificate giftCertificate = giftCertificateRepository.findById(id)
                .orElseThrow(GiftCertificateNotFoundException::new);

        if (giftCertificateDto.getName() != null) {
            verifyGiftCertificateName(giftCertificateDto.getName());
            giftCertificate.setName(giftCertificateDto.getName());
        }
        if (giftCertificateDto.getDescription() != null) {
            giftCertificate.setDescription(giftCertificateDto.getDescription());
        }
        if (giftCertificateDto.getDuration() != null) {
            giftCertificate.setDuration(giftCertificateDto.getDuration());
        }
        if (giftCertificateDto.getPrice() != null) {
            giftCertificate.setPrice(giftCertificateDto.getPrice());
        }
        if (giftCertificateDto.getTags() != null) {
            giftCertificate.setTags(null);
            giftCertificate.setTags(tagService.saveNotExistingTags(giftCertificateDto.getTags()));
        }
        giftCertificate.setLastUpdateDate(LocalDateTime.now());
        GiftCertificateDto dto = giftCertificateMapper.toDto(giftCertificate);

        kafkaTemplate.send(GIFT_CERTIFICATE_TOPIC, giftCertificateMapper.toRequestDto(giftCertificate));
        return dto;
    }

    /**
     * Delete a gift certificate from the database and cache by id.
     * @param id unique id of gift certificate.
     */
    @CacheEvict(cacheNames = GIFT_CERTIFICATE_DTO_KEY, key = "#id")
    @Override
    public void delete(Long id) {
        if (!giftCertificateRepository.existsById(id)) {
            throw new GiftCertificateNotFoundException();
        }
        giftCertificateRepository.deleteById(id);
    }

    private void verifyGiftCertificateName(String name) {
        if (name.trim().isEmpty()) {
            throw new GiftCertificateWrongField();
        }
        if (giftCertificateRepository.existsByNameIgnoreCase(name)) {
            throw new GiftCertificateNotUniqueNameException();
        }
    }

    private void verifyGiftCertificateFields(GiftCertificateDto giftCertificateDto) {
        boolean allFieldNotNull = giftCertificateDto.getName() != null &&
                giftCertificateDto.getDescription() != null &&
                giftCertificateDto.getDuration() != null &&
                giftCertificateDto.getPrice() != null &&
                giftCertificateDto.getTags() != null;
          if (!allFieldNotNull) {
            throw new GiftCertificateWrongField();
        }
    }
}
