package com.epam.esm.service.impl;

import com.epam.esm.dao.entity.Tag;
import com.epam.esm.dao.repository.TagRepository;
import com.epam.esm.dto.TagDto;
import com.epam.esm.dto.TagRequestDto;
import com.epam.esm.exception.TagNotFoundException;
import com.epam.esm.exception.TagNotUniqueNameException;
import com.epam.esm.mapper.TagMapper;
import com.epam.esm.service.TagService;
import jakarta.annotation.PostConstruct;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.epam.esm.util.CacheConstants.TAG_DTO_KEY;
import static com.epam.esm.util.CacheConstants.TAG_DTO_KEY_ALL;
import static com.epam.esm.util.KafkaConstants.TAG_TOPIC;
import static com.epam.esm.util.RedissonConstants.REDISSON_TAG;

/**
 * The class represents service methods for gift certificates with work at the dao level.
 */
@Service
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final TagMapper tagMapper;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final RedissonClient redissonClient;
    private final CacheManager cacheManager;
    private RList<TagRequestDto> rList;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository,
                          TagMapper tagMapper,
                          @Qualifier("tagKafkaTemplate") KafkaTemplate<String, Object> kafkaTemplate,
                          RedissonClient redissonClient, CacheManager cacheManager) {
        this.tagRepository = tagRepository;
        this.tagMapper = tagMapper;
        this.kafkaTemplate = kafkaTemplate;
        this.redissonClient = redissonClient;
        this.cacheManager = cacheManager;
    }

    @PostConstruct
    private void init() {
        rList = redissonClient.getList(REDISSON_TAG);
    }

    @Override
    public RList<TagRequestDto> getrList() {
        return rList;
    }

    /**
     * Get and cache tag by id.
     *
     * @param id unique id of tag.
     * @return {@link  TagDto}
     */
    @Cacheable(cacheNames = TAG_DTO_KEY, key = "#id")
    @Override
    public TagDto get(Long id) {
        return tagMapper.toDto(tagRepository.findById(id).orElseThrow(TagNotFoundException::new));
    }

    /**
     * Get and cache all tags.
     *
     * @return {@link  List<TagDto>}
     */
    @Cacheable(cacheNames = TAG_DTO_KEY_ALL)
    @Override
    public List<TagDto> getAll() {
        return tagMapper.toDto(tagRepository.findAll());
    }

    /**
     * Add a tag to the database.
     * Clears the cache of all tags because the cache value has been changed
     * For educational purposes: send the tag as {@link TagRequestDto} using {@link KafkaTemplate}
     * to listener {@link com.epam.esm.kafka.listener.Listener}
     * where it saves into Redis database as {@link RList}
     *
     * @param tagDto New tag.
     * @return {@link  TagDto} Saved tag.
     */
    @Override
    public TagDto add(TagDto tagDto) {
        verifyTagName(tagDto.getName());
        Tag saved = tagRepository.save(tagMapper.toEntity(tagDto));
        TagDto savedTagDto = tagMapper.toDto(saved);
        kafkaTemplate.send(TAG_TOPIC, tagMapper.toRequestDto(saved));
        evictAllCacheValues(TAG_DTO_KEY_ALL);
        return savedTagDto;
    }

    private Set<Tag> addAll(Set<TagDto> tagDtoSet) {
        tagDtoSet.forEach(tag -> verifyTagName(tag.getName()));
        return new HashSet<>((Collection<Tag>) tagRepository.saveAll(tagMapper.toEntity(tagDtoSet)));
    }

    /**
     * Delete a tag from the database and cache by id.
     *
     * @param id unique id of gift certificate.
     */
    @Override
    public void delete(Long id) {
        if (!tagRepository.existsById(id)) {
            throw new TagNotFoundException();
        }
        tagRepository.deleteById(id);
        evictSingleCacheValue(TAG_DTO_KEY, id);
    }

    /**
     * Get all tags by gift certificate id.
     *
     * @param id Gift certificate id.
     * @return {@link  Set<TagDto>}
     */
    @Override
    public Set<TagDto> findAllByGiftCertificateId(Long id) {
        return tagMapper.toDto(tagRepository.findAllByGiftCertificateId(id));
    }

    /**
     * Saves those tags that are not in the database and returns all the requested tags.
     *
     * @param tagDtoSet Tags to save.
     * @return {@link  Set<Tag>} all the requested tags.
     */
    @Override
    public Set<Tag> saveNotExistingTags(Set<TagDto> tagDtoSet) {
        Set<Tag> existingTags = getByNames(tagDtoSet);
        Set<Tag> newTags = addAll(tagDtoSet.stream()
                .filter(tagDto -> !existingTags.stream()
                        .map(Tag::getName)
                        .collect(Collectors.toSet())
                        .contains(tagDto.getName()))
                .collect(Collectors.toSet()));
        existingTags.addAll(newTags);
        newTags.forEach(tag -> kafkaTemplate.send(TAG_TOPIC, tagMapper.toRequestDto(tag)));
        return existingTags;
    }

    private Set<Tag> getByNames(Set<TagDto> tagDtoSet) {
        return tagRepository.getAllByNameIn(tagDtoSet.stream().map(TagDto::getName).collect(Collectors.toSet()));
    }

    private void verifyTagName(String name) {
        if (tagRepository.existsByNameIgnoreCase(name)) {
            throw new TagNotUniqueNameException();
        }
    }

    private void evictSingleCacheValue(String cacheName, Long cacheKey) {
        cacheManager.getCache(cacheName).evict(cacheKey);
    }

    private void evictAllCacheValues(String cacheName) {
        cacheManager.getCache(cacheName).clear();
    }

}
