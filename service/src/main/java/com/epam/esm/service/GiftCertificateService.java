package com.epam.esm.service;

import com.epam.esm.dao.entity.GiftCertificate;
import com.epam.esm.dto.GiftCertificateDto;
import com.epam.esm.dto.GiftCertificateRequestDto;
import org.redisson.api.RList;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface GiftCertificateService {
    RList<GiftCertificateRequestDto> getrList();

    GiftCertificateDto get(Long id);

    List<GiftCertificateDto> getAll();

    List<GiftCertificateDto> findAll(Specification<GiftCertificate> query);

    GiftCertificateDto add(GiftCertificateDto giftCertificateRequest);

    GiftCertificateDto update(Long id, GiftCertificateDto giftCertificateRequest);

    void delete(Long id);

}
