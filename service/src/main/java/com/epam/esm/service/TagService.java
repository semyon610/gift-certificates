package com.epam.esm.service;

import com.epam.esm.dao.entity.Tag;
import com.epam.esm.dto.TagDto;
import com.epam.esm.dto.TagRequestDto;
import org.redisson.api.RList;

import java.util.List;
import java.util.Set;


public interface TagService {

    RList<TagRequestDto> getrList();

    TagDto get(Long id);

    List<TagDto> getAll();

    TagDto add(TagDto tagDto);

    void delete(Long id);

    Set<TagDto> findAllByGiftCertificateId(Long id);

    Set<Tag> saveNotExistingTags(Set<TagDto> tagDtoSet);


}
