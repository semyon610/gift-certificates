package com.epam.esm.util;

import lombok.experimental.UtilityClass;

/**
 * Contains the names of Redis storages.
 */
@UtilityClass
public class RedissonConstants {
    public static final String REDISSON_TAG = "redis_tag";
    public static final String REDISSON_GIFT_CERTIFICATE = "redis_gift_certificate";

}
