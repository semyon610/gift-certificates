package com.epam.esm.util;

import lombok.experimental.UtilityClass;

/**
 * Contains the names of Kafka topics.
 */
@UtilityClass
public class KafkaConstants {
    public final static String TAG_TOPIC = "kafka_tag";
    public final static String GIFT_CERTIFICATE_TOPIC = "kafka_gift_certificate";

}
