package com.epam.esm.util;

import lombok.experimental.UtilityClass;

/**
 * Contains the names of Redis storages used for caching.
 */
@UtilityClass
public class CacheConstants {
    public static final String TAG_DTO_KEY = "TagDtoKey";
    public static final String TAG_DTO_KEY_ALL = "TagDtoKey";
    public static final String GIFT_CERTIFICATE_DTO_KEY = "GiftCertificateDtoKey";
    public static final String GIFT_CERTIFICATE_DTO_KEY_ALL = "GiftCertificateDtoKey";

}
