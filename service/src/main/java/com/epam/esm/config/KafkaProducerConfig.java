package com.epam.esm.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@code KafkaProducerConfig} class contains the configuration of the consumer
 */
@Configuration
public class KafkaProducerConfig {
    @Value("${kafka.bootstrap.server}")
    private String kafkaServer;

    /**
     * Sets the producer's properties
     *
     * @return {@link Map} The key is the name of the property, values contains custom settings
     */
    private Map<String, Object> producerConfigs(String clientId) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        configs.put(ProducerConfig.CLIENT_ID_CONFIG, clientId);
        configs.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 127000);
        return configs;
    }

    /**
     * The method is required to register a class with a specific client ID value.
     * <p>
     * The {@link KafkaTemplate} class produces messages in the Kafka topics.
     * </p>
     *
     * @param clientId Configuration defining the {@code KafkaTemplate} class
     * @return {@link KafkaTemplate}
     */
    private KafkaTemplate<String, Object> kafkaTemplate(String clientId) {
        ProducerFactory<String, Object> producerFactory = new DefaultKafkaProducerFactory<>(producerConfigs(clientId));
        KafkaTemplate<String, Object> template = new KafkaTemplate<>(producerFactory);
        template.setMessageConverter(new StringJsonMessageConverter());
        return template;
    }

    /**
     * Configuration of the server where Kafka topics are stored.
     *
     * @return {@link KafkaAdmin}
     */
    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
        return new KafkaAdmin(configs);
    }

    /**
     * Initializes class {@link KafkaTemplate} for tags.
     *
     * @return {@link KafkaTemplate}
     */
    @Bean
    public KafkaTemplate<String, Object> tagKafkaTemplate() {
        return kafkaTemplate("tag");
    }

    /**
     * Initializes class {@link KafkaTemplate} for gift certificates.
     *
     * @return {@link KafkaTemplate}
     */
    @Bean
    public KafkaTemplate<String, Object> certKafkaTemplate() {
        return kafkaTemplate("gift_certificate");
    }
}

