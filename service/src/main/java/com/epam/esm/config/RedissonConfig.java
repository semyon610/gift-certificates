package com.epam.esm.config;

import jodd.util.StringUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.redisson.config.Config;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

import static com.epam.esm.util.CacheConstants.*;

/**
 * The {@code RedissonConfig} class configures the {@link RedissonClient} and {@link CacheManager} classes.
 */
@Configuration
@EnableCaching
public class RedissonConfig {
    public static final int TWENTY_SECOND = 1000 * 20;
    public static final int ONE_MINUTE = 1000 * 60;
    @Value("${redis.address}")
    private String REDIS_PATH;

    /**
     * Configuring data serialization and server address
     *
     * @return {@link RedissonClient}
     */
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.setCodec(new SerializationCodec());
        if (StringUtil.isNotBlank(REDIS_PATH)) {
            config.useSingleServer().setAddress(REDIS_PATH);
        }
        RedissonClient redissonClient = Redisson.create(config);
        if (redissonClient.getKeys().count() != 0)
            redissonClient.getKeys().flushdb();
        return redissonClient;
    }

    /**
     * Configuring the request lifetime and the lifetime between requests for each cache store.
     *
     * @param redissonClient Provides storage to the cache manager.
     * @return {@link CacheManager}
     */
    @Bean
    CacheManager cacheManager(RedissonClient redissonClient) {
        Map<String, CacheConfig> config = new HashMap<>();
        config.put(TAG_DTO_KEY, new CacheConfig(ONE_MINUTE, TWENTY_SECOND));
        config.put(TAG_DTO_KEY_ALL, new CacheConfig(ONE_MINUTE, TWENTY_SECOND));
        config.put(GIFT_CERTIFICATE_DTO_KEY, new CacheConfig(ONE_MINUTE, TWENTY_SECOND));
        config.put(GIFT_CERTIFICATE_DTO_KEY_ALL, new CacheConfig(ONE_MINUTE, TWENTY_SECOND));
        return new RedissonSpringCacheManager(redissonClient, config);
    }
}
