package com.epam.esm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A class represents the dto class for the {@code Tag} class
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TagDto implements Serializable {

    private static final long serialVersionUID = -8791263702511283660L;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;
    @Size(min = 1, max = 100)
    @NotBlank
    @Pattern(regexp = "^\\S+$", message = "The tag name must not contain spaces")
    private String name;
}
