package com.epam.esm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * A class represents the dto class used for requests for the {@code GiftCertificate} class
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GiftCertificateRequestDto implements Serializable {
    private static long serialVersionUID = -7813788805508283965L;
    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer duration;
    private LocalDateTime createDate;
    private LocalDateTime lastUpdateDate;
    private Set<TagRequestDto> tags;
}
