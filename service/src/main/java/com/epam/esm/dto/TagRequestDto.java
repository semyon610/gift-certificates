package com.epam.esm.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * A class represents the dto class used for requests for the {@code Tag} class
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TagRequestDto implements Serializable {
    private static final long serialVersionUID = -6414208766034548611L;
    private Long id;
    private String name;
}
