package com.epam.esm.kafka;

import com.epam.esm.dto.TagRequestDto;
import jakarta.annotation.PostConstruct;
import org.redisson.api.RList;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * The service was created for experimental purposes.
 * Here 2 consumers from different groups read messages from the same topic.
 */
@Service
public class KafkaTagServiceExample {
    private final RedissonClient redissonClient;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private RList<TagRequestDto> rList1;
    private RList<TagRequestDto> rList2;
    private Long i = 1L;
    private final static String FIRST_TAG_GROUP_NAME = "TAG1";
    private final static String SECOND_TAG_GROUP_NAME = "TAG2";
    public final static String TAG_TOPIC = "tagTopic";
    private final static String TAG_NAME = "tagName";

    @Autowired
    public KafkaTagServiceExample(RedissonClient redissonClient,
                                      @Qualifier("tagKafkaTemplate") KafkaTemplate<String, Object> kafkaTemplate) {
        this.redissonClient = redissonClient;
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostConstruct
    private void init() {
        rList1 = redissonClient.getList(FIRST_TAG_GROUP_NAME);
        rList2 = redissonClient.getList(SECOND_TAG_GROUP_NAME);
    }

    public RList<TagRequestDto> getRList1() {
        return rList1;
    }

    public RList<TagRequestDto> getRList2() {
        return rList2;
    }

//    @Scheduled(fixedDelay = 3000)
//    @Override
//    public void send() {
//        TagDto tagDto = TagDto.builder().id(++i).name(TAG_NAME).build();
//        kafkaTemplate.send(TAG_TOPIC, tagDto);
//    }

}
