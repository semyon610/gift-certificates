package com.epam.esm.kafka.listener;

import com.epam.esm.dto.GiftCertificateRequestDto;
import com.epam.esm.dto.TagRequestDto;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.TagService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.redisson.api.RList;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import static com.epam.esm.util.KafkaConstants.GIFT_CERTIFICATE_TOPIC;
import static com.epam.esm.util.KafkaConstants.TAG_TOPIC;

/**
 * The class represents service methods that read messages from different topics.
 */
@RequiredArgsConstructor
@Service
public class Listener {
    private final TagService tagService;
    private final GiftCertificateService giftCertificateService;

    /**
     * The method reads the {@link TagRequestDto} and stores it in the Redis storage as {@link RList}.
     *
     * @param message {@code TagRequestDto}
     * @throws JsonProcessingException Happens when a non-instance of the {@code TagRequestDto} class was received
     */
    @KafkaListener(topics = TAG_TOPIC, groupId = "app", containerFactory = "defaultContainerFactory")
    public void listenTag(@Payload ConsumerRecord<?, ?> message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        TagRequestDto tagDto = objectMapper.readValue(String.valueOf(message.value()), TagRequestDto.class);
        RList<TagRequestDto> dtoRList = tagService.getrList();
        dtoRList.add(tagDto);
    }

    /**
     * The method reads the {@link GiftCertificateRequestDto} and stores it in the Redis storage as {@link RList}.
     *
     * @param message {@code GiftCertificateRequestDto}
     * @throws JsonProcessingException Happens when a non-instance of the {@code GiftCertificateRequestDto} class was received
     */
    @KafkaListener(topics = GIFT_CERTIFICATE_TOPIC, groupId = "app", containerFactory = "defaultContainerFactory")
    public void listenGiftCertificate(@Payload ConsumerRecord<?, ?> message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        GiftCertificateRequestDto dto = objectMapper.readValue(String.valueOf(message.value()), GiftCertificateRequestDto.class);
        RList<GiftCertificateRequestDto> dtoRList = giftCertificateService.getrList();
        dtoRList.add(dto);
    }
}
