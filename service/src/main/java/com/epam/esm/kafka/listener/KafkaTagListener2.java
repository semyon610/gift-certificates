package com.epam.esm.kafka.listener;

import com.epam.esm.dto.TagRequestDto;
import com.epam.esm.kafka.KafkaTagServiceExample;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.redisson.api.RList;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

/**
 * The service was created for experimental purposes.
 * The class represents service methods that read messages from a tag topic.
 */
@RequiredArgsConstructor
@Service
public class KafkaTagListener2 {
    private final KafkaTagServiceExample tagService;

    /**
     * The method reads the {@link TagRequestDto} and stores it in the Redis storage as {@link RList}.
     *
     * @param message {@code TagRequestDto}
     * @throws JsonProcessingException Happens when a non-instance of the {@code TagRequestDto} class was received
     */
    @KafkaListener(topics = "tagTopic", groupId = "tag2", containerFactory = "defaultContainerFactory")
    public void listen(@Payload ConsumerRecord<?, ?> message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        TagRequestDto tagDto = objectMapper.readValue(String.valueOf(message.value()), TagRequestDto.class);
        RList<TagRequestDto> dtoRList = tagService.getRList2();
        dtoRList.add(tagDto);
    }
}
