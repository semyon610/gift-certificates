package com.epam.esm.exception;

/**
 * an exception is thrown when an attempt is made to set the name of a gift certificate that already exists
 */
public class GiftCertificateNotUniqueNameException extends RuntimeException {
    public GiftCertificateNotUniqueNameException() {
        super("This gift certificate name already exists");
    }

}
