package com.epam.esm.exception;

/**
 * The exception is thrown when an attempt is made to set the name of a tag that already exists
 */
public class TagNotUniqueNameException extends RuntimeException {
    public TagNotUniqueNameException() {
        super("This tag name already exists");
    }
}
