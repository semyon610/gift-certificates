package com.epam.esm.exception;

/**
 * The exception is thrown when the tag name is not found
 */
public class TagNotFoundException extends RuntimeException {
    public TagNotFoundException() {
        super("Tag not found");
    }
}
