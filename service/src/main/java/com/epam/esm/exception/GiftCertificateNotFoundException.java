package com.epam.esm.exception;

/**
 * The exception is thrown when the gift certificate is not found
 */
public class GiftCertificateNotFoundException extends RuntimeException {
    public GiftCertificateNotFoundException() {
        super("Gift certificate not found");
    }
}
