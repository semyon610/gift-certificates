package com.epam.esm.exception;

/**
 * The exception is thrown when not all fields are defined during the request
 */
public class GiftCertificateWrongField extends RuntimeException {
    public GiftCertificateWrongField() {
        super("Invalid request content");
    }

}
