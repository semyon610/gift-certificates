package com.epam.esm.dao.repository;

import com.epam.esm.dao.entity.GiftCertificate;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *  The interface provides a repository for the {@link GiftCertificate} class
 */
@Repository
public interface GiftCertificateRepository extends CrudRepository<GiftCertificate, Long>,
        JpaSpecificationExecutor<GiftCertificate>{
    List<GiftCertificate> findAll();

    boolean existsByNameIgnoreCase(String name);

    Optional<GiftCertificate> findById(Long id);

    void deleteById(Long id);
}
