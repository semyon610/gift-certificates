package com.epam.esm.dao.repository;

import com.epam.esm.dao.entity.Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *  The interface provides a repository for the {@link Tag} class
 */
@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {

    List<Tag> findAll();

    boolean existsByNameIgnoreCase(String name);

    Optional<Tag> findById(Long id);

    void deleteById(Long id);

    Set<Tag> getAllByNameIn(Set<String> tagNames);

    @Query(value = "select * from tag t " +
            "join gift_certificate_tag gct on t.id = gct.tag_id " +
            "where gct.gift_certificate_id=?1",
            nativeQuery = true)
    Set<Tag> findAllByGiftCertificateId(Long id);

}
